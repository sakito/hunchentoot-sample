#|
Copyright (c) 2012 sakito <sakito@sakito.com>

This program is licensed under the terms of the LLGPL.
|#

(in-package :cl-user)
(defpackage hunchentoot-sample-asd
  (:use :cl :asdf)
  (:export :start :stop))
(in-package :hunchentoot-sample-asd)

(defsystem hunchentoot-sample
  :version "0.0"
  :author "sakito"
  :license "LLGPL"
  :serial t
  :depends-on (:hunchentoot
               :alexandria
               :clsql
               :cl-markup)
  :components ((:file "hunchentoot-sample")))
